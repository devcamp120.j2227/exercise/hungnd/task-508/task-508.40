// Import thư viện expressjs tương đương import express from "express";
const express = require("express");
// Import thư viện path
const path = require("path");
// Khởi tạo 1 app express
const app = express();
// khai báo thư viện mongoose
const mongoose = require("mongoose");
const drinkRouter = require("./app/routes/drinkRouter");
const voucherRouter = require("./app/routes/voucherRouter");

// Cấu hình request đọc được body json
app.use(express.json());

// khai báo cổng chạy project
const port = 8000;
//Callback function là 1 function đóng vài trò là tham số của 1 function khác, nó sẽ thực hiện khi function chủ được gọi
// Khai báo API dạng /
app.get("/", (req, res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/views/Pizza 365 v1.9 Menu.html"));
});
// Kết nối với mongo
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365", (error) => {
    if (error) throw error;
    console.log('Successfully connected');
});
// App sử dụng router
app.use('/api', drinkRouter);
app.use('/api', voucherRouter);

app.use(express.static(__dirname + "/views"));
app.use((req, res, next) => {
    console.log("Current time: ", new Date());
    next();
})
app.listen(port, () => {
    console.log("App listening on port: ", port);
});