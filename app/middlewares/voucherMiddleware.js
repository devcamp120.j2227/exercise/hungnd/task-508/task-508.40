const getAllVoucherMiddleware = (req, res, next) => {
    console.log("Get All Voucher!");
    next();
};

const getVoucherMiddleware = (req, res, next) => {
    console.log("Get Voucher By Id!");
    next();
};

const postVoucherMiddleware = (req, res, next) => {
    console.log("Create new Voucher!");
    next();
};

const putVoucherMiddleware = (req, res, next) => {
    console.log("Update Voucher!");
    next();
};

const deleteVoucherMiddleware = (req, res, next) => {
    console.log("Delete Voucher!");
    next();
};

module.exports = {
    getAllVoucherMiddleware,
    getVoucherMiddleware,
    postVoucherMiddleware,
    putVoucherMiddleware,
    deleteVoucherMiddleware
};