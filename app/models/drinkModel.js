// Import thư viện mongoose
const mongoose = require('mongoose');

// khai báo class Schema từ thư viện mogoose
const Schema = mongoose.Schema;

//Khởi tạo instance drinkSchema từ Class Schema
const drinkSchema = new Schema({
    maNuocUong: {
        type: String,
        unique: true,
        required: true
    },
    tenNuocUong: {
        type: String,
        unique: true,
        required: true
    },
    donGia: {
        type: Number,
        required: true
    }
},
{
    timestamps: true
})

module.exports = mongoose.model('Drink', drinkSchema);
