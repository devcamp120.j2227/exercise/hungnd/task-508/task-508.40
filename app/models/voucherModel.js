// Import thư viện mogoose
const mongoose = require('mongoose');

// Khai báo class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

// Khởi tạo instance voucherSchema từ class Schema

const voucherSchema = new Schema({
    maVoucher: {
        type: String,
        unique: true,
        required: true
    },
    phanTramGiamGia: {
        type: Number,
        required: true
    },
    ghiChu: {
        type: String,
        required: false
    }
}, {
    timestamps: true
})
module.exports = mongoose.model('Voucher', voucherSchema);