// Import thư viện mongoose
const mongoose = require('mongoose');

// khai báo class Schema từ thư viện mogoose
const Schema = mongoose.Schema;

//Khởi tạo instance userSchema từ Class Schema
const userSchema = new Schema({
    fullName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        unique: true,
        required: true
    },
    orders: [{
        type: mongoose.Types.ObjectId,
        ref: "Order"
    }]
},
{
    timestamps: true
})

module.exports = mongoose.model('User', userSchema);
