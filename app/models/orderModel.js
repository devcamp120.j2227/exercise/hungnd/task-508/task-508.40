// Import thư viện mongoose
const mongoose = require('mongoose');

// khai báo class Schema từ thư viện mogoose
const Schema = mongoose.Schema;

//Khởi tạo instance orderSchema từ Class Schema
const orderSchema = new Schema({
    orderCode: {
        type: String,
        unique: true
    },
    pizzaSize: {
        type: String,
        required: true
    },
    pizzaType: {
        type: String,
        required: true
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: "Voucher"
    },
    drink: {
        type: mongoose.Types.ObjectId,
        ref: "Drink"
    },
    status: {
        type: String,
        required: true
    }
},
{
    timestamps: true
})

module.exports = mongoose.model('Order', orderSchema);
