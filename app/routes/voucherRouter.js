// Import thư viện express
const express = require('express');

// Khai báo voucherRouter app
const voucherRouter = express.Router();

// Import voucher middleware
const voucherMiddleware = require('../middlewares/voucherMiddleware');

// Import voucher Controller
const voucherController = require('../controllers/voucherController');

voucherRouter.post('/vouchers', voucherMiddleware.postVoucherMiddleware, voucherController.createVoucher);

module.exports = voucherRouter;