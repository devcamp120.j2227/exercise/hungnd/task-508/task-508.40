// Khai báo thư viện express
const express =  require('express');

// Khai báo router app
const drinkRouter = express.Router();

// Import drink middleware
const drinkMiddleware = require('../middlewares/drinkMiddleware');

//Import drink Controller
const drinkController = require('../controllers/drinkController');

drinkRouter.post("/drinks", drinkMiddleware.postDrinkMiddleware, drinkController.createDrink);
drinkRouter.get("/drinks", drinkMiddleware.getAllDrinkMiddleware, drinkController.getAllDrink);
drinkRouter.get("/drinks/:drinkId", drinkMiddleware.getDrinkMiddleware, drinkController.getDrinkById);
drinkRouter.put("/drinks/:drinkId", drinkMiddleware.putDrinkMiddleware, drinkController.updateDrinkById);
drinkRouter.delete("/drinks/:drinkId", drinkMiddleware.deleteDrinkMiddleware, drinkController.deleteDrinkById);

module.exports = drinkRouter;