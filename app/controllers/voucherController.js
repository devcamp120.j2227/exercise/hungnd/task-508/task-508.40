// Import thư viện mongoose
const mongoose = require('mongoose');

// Import voucherModel
const voucherModel = require('../models/voucherModel');

// Create new voucher
const createVoucher = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    const body = req.body;
    console.log(body);
    //B2: Kiểm tra dữ liệu
    if(!body.maVoucher) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "maVoucher không hợp lệ!"
        })
    }
    if(!body.phanTramGiamGia) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "phanTramGiamGia không hợp lệ!"
        })
    }
    //B3: Gọi Model tạo dữ liệu
    const newVoucher = {
        _id: mongoose.Types.ObjectId(),
        maVoucher: body.maVoucher,
        phanTramGiamGia: body.phanTramGiamGia,
        ghiChu: body.ghiChu
    }
    voucherModel.create(newVoucher, (err, data) => {
        if(err) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }
        return res.status(201).json({
            message: "Create new voucher successfully!",
            data: data
        })
    })
}

module.exports = {
    createVoucher
}