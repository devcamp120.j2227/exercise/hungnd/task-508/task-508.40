// Import thư viện mongoose
const mongoose = require('mongoose');

// Import drinkModel
const drinkModel = require('../models/drinkModel');

// Create new Drink
const createDrink = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    const body = req.body;
    console.log(body);
    // B2: validate dữ liệu
    // Kiểm tra maNuocUong
    if(!body.maNuocUong) {
        return res.status(400).json({
            status: "Bad request",
            message: "maNuocUong không hợp lệ"
        })
    }
    // Kiểm tra tenNuocUong
    if(!body.tenNuocUong) {
        return res.status(400).json({
            status: "Bad request",
            message: "tenNuocUong không hợp lệ"
        })
    }
    // Kiểm tra donGia
    if(!body.donGia || isNaN(body.donGia)) {
        return res.status(400).json({
            status: "Bad request",
            message: "donGia không hợp lệ"
        })
    }
    // B3: Gọi model tạo dữ liệu
    const newDrink = {
        _id: mongoose.Types.ObjectId(),
        maNuocUong: body.maNuocUong,
        tenNuocUong: body.tenNuocUong,
        donGia: body.donGia
    }
    drinkModel.create(newDrink, (err, data) => {
        if(err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(201).json({
            message: "Create new drink successfully!",
            data: data
        })
    })
}

// get all drink
const getAllDrink = (req, res) => {
    drinkModel.find((err, data) => {
        if(err) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }
        return res.status(201).json({
            message: "Get all drink successfully!",
            data: data
        })
    })
}
// get a drink by id
const getDrinkById = (req, res) => {
    //B1: Thu thập dữ liệu
    const drinkId = req.params.drinkId;
    console.log(drinkId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "Drink Id is not valid!"
        })
    }
    // B3: Thực hiện load drink theo id
    drinkModel.findById(drinkId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Get drink by Id successfully!",
                data: data
            })
        }
    })
};
// Update a drink
const updateDrinkById = (req, res) => {
    //B1: Thu thập dữ liệu
    const drinkId = req.params.drinkId;
    console.log(drinkId);
    const body = req.body;
    console.log(body);
    //B2: Kiểm tra dữ liệu
    // KIểm tra course Id
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "Drink Id is not valid!"
        })
    }
    // Kiểm tra maNuocUong
    if (!body.maNuocUong) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "maNuocUong is not valid!"
        });
    }
    // Kiểm tra tenNuocUong
    if (!body.tenNuocUong) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "tenNuocUong is not valid!"
        })
    }
    //Kiểm tra donGia
    if (!body.donGia || body.donGia < 0) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "donGia is not valid!"
        })
    }
    //B3: thực hiện tạo mới drink
    let newDrink = {
        maNuocUong: body.maNuocUong,
        tenNuocUong: body.tenNuocUong,
        donGia: body.donGia
    }
    drinkModel.findByIdAndUpdate(drinkId, newDrink, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Update drink by Id successfully!",
                data: data
            })
        }
    })
};
// delete a drink
const deleteDrinkById = (req, res) => {
    //B1: Thu thập dữ liệu
    const drinkId = req.params.drinkId;
    console.log(drinkId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            status: "Error 400: bad request",
            message: "Drink Id is not valid!"
        })
    }
    // B3: Thực hiện xóa drink theo id
    drinkModel.findByIdAndDelete(drinkId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: internal server error",
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                status: "Delete drink by Id successfully!",
                data: data
            })
        }
    })
};
module.exports = {
    createDrink,
    getAllDrink,
    getDrinkById,
    updateDrinkById,
    deleteDrinkById
}